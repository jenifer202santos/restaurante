<?php
require_once('../Controle/Conect.php');
  
session_start();


if(isset($_SESSION['user'])){
  echo"<!doctype html>
  <html lang='pt-br'> 
    <head>
      <!-- Required meta tags -->
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/bootstrap.css' />
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/espFesta.css' />
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/style.css'/>
      <title> Reservar Espaço de Festa </title> <link  rel='shortcut icon'  href='../imagens/logo1.ico'  type='image/x-icon' >
    </head>
    <body style='background-color:#ececec;'>
    	<div class='row topo'> 


    		<div class='col-lg-12'> <img src='../imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


    	</div>


  <nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
  <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
      <img src='../imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
    </button>

    <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
      <ul class='navbar-nav'>
        <li class='nav-item active'>
          <a class='nav-link px-md-4' href='../index.php' style='color: white;'>Home</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link text-white px-md-4' href='Cardapio.php'>Cardápio</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link text-white px-md-4' href='Atracoes.php'>Atrações </a>
        </li>
        <li class='nav-item dropdown'>
          <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            Reservar
          </a>
          <div class='dropdown-menu' aria-labelledby='dropdown08'>
            <a class='dropdown-item' href='Mesas.php'> Mesas </a>
            <a class='dropdown-item' href='EspFesta.php'> Espaço de Festa</a>
          </div>
        </li>

        <li class='nav-item dropdown'>
          <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            Entrar
          </a>
          <div class='dropdown-menu' aria-labelledby='dropdown08'>
            <a class='dropdown-item' href='Cadastro.php'> Cadastre - se </a>
            <a class='dropdown-item' href='Login.php'>  Login </a>
          </div>
        </li>
      </ul>
    </div>
  </nav>

  <div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
    <div class='carousel-inner'>
      <div class='carousel-item active'>
        <img src='../imagens/espFesta.png' class='d-block w-100' alt='...''>
      </div>

    </div>
  </div>

 <div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Espaço de Festa na Fazenda</h1>
           
            <p class='lead'> Se localiza em uma fazenda, é capaz de proporcionar uma experiência junto a natureza e oferece um bifé com todas as iguarias rupestres. Além disso, lhe fornecemos um local onde ficaria protegido de chuvas ou de qualquer outro imprevisto. </p>
            <a  href='reservarE1.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Reservar</button> </a>
          
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp12.jpg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
        </div>  

      <hr class='featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp6.jpeg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Espaço de Festa Simples</h1>
           
            <p class='lead'>Se localiza na varanda do nosso restaurante, é um local onde a maior parte da decoração é feita com flores, passando um ar de mais liberdade. Além de iguarias rupestres, iriamos oferecer pratos especiais do nosso restaurante. Assim como o espaço da fazenda, é um local onde ficaria protegido de chuvas ou qualquer outro imprevisto.</p>
             <a  href='reservarE2.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Reservar</button> </a>
          
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Espaço de Festa Clássico </h1>
           
             <p class='lead'>Se localiza no nosso salão principal, onde é recomendado para festa de debutantes, festa de casamento ou baile de formatura. Fornece pratos mais clássicos e mais delicados, além de que a decoração seria a mais elegante de todas. Além disso, o serviço de garções estaria disponivel, porém com um maior preço. </p>
              <a  href='reservarE3.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Reservar</button> </a>
          
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp7.jpeg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp4.jpeg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'>Espaço de Festa ao Ar Livre</h1>
            
            <p class='lead'>Se localiza no jardim do restaurante, contaria também com pratos do nosso restaurante para lhe fornecer um melhor serviço. Possue uma decoração com muitas flores, mas também com mesas clássicas. Além disso, o local lhe fornece uma vista para a nossa fonte e lhe dá um local muito extensivo, onde caberia a maior quantidade de pessoas, e estaria protegido de chuvas ou outros imprevistos.</p>
             <a  href='reservarE4.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Reservar</button> </a>
          
          </div>
        </div>
     
<br>
</div>
<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded float-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded float-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
            
</div>
</nav>

 




      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
  <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
  <script type='text/javascript' src='../bootstrap/js/pace.min.js'></script>
    </body>
  </html>";

}else{
  if(isset($_SESSION['adm'])){
    echo "<!doctype html>
  <html lang='pt-br'> 
    <head>
      <!-- Required meta tags -->
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/bootstrap.css' />
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/espFesta.css' />
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/style.css'/>
      <title> Reservar Espaço de Festa </title> <link  rel='shortcut icon'  href='../imagens/logo1.ico'  type='image/x-icon' >
    </head>
    <body style='background-color:#ececec;'>
      <div class='row topo'> 


        <div class='col-lg-12'> <img src='../imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


      </div>


  <nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
  <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
      <img src='../imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
    </button>

    <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
      <ul class='navbar-nav'>
        <li class='nav-item active'>
          <a class='nav-link px-md-4' href='../index.php' style='color: white;'>Home</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link text-white px-md-4' href='Cardapio.php'>Cardápio</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link text-white px-md-4' href='Atracoes.php'>Atrações </a>
        </li>
        <li class='nav-item dropdown'>
          <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            Reservar
          </a>
          <div class='dropdown-menu' aria-labelledby='dropdown08'>
            <a class='dropdown-item' href='Mesas.php'> Mesas </a>
            <a class='dropdown-item' href='EspFesta.php'> Espaço de Festa</a>
          </div>
        </li>
          <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='upAdm.php'>Modificar Adm</a>
      </li>
     <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='../Controle/sair.php'>Sair</a>
      </li>
        </li>
      </ul>
    </div>
  </nav>

  <div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
    <div class='carousel-inner'>
      <div class='carousel-item active'>
        <img src='../imagens/espFesta.png' class='d-block w-100' alt='...''>
      </div>

    </div>
  </div>

 <div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Espaço de Festa na Fazenda</h1>
           
            <p class='lead'> Se localiza em uma fazenda, é capaz de proporcionar uma experiência junto a natureza e oferece um bifé com todas as iguarias rupestres. Além disso, lhe fornecemos um local onde ficaria protegido de chuvas ou de qualquer outro imprevisto. </p>
            <a  href='visuE1.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Ver Dados</button> </a>
          
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp12.jpg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
        </div>  

      <hr class='featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp6.jpeg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Espaço de Festa Simples</h1>
           
            <p class='lead'>Se localiza na varanda do nosso restaurante, é um local onde a maior parte da decoração é feita com flores, passando um ar de mais liberdade. Além de iguarias rupestres, iriamos oferecer pratos especiais do nosso restaurante. Assim como o espaço da fazenda, é um local onde ficaria protegido de chuvas ou qualquer outro imprevisto.</p>
             <a  href='visuE2.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Ver Dados</button> </a>
          
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Espaço de Festa Clássico </h1>
           
             <p class='lead'>Se localiza no nosso salão principal, onde é recomendado para festa de debutantes, festa de casamento ou baile de formatura. Fornece pratos mais clássicos e mais delicados, além de que a decoração seria a mais elegante de todas. Além disso, o serviço de garções estaria disponivel, porém com um maior preço. </p>
              <a  href='visuE3.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Ver Dados</button> </a>
          
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp7.jpeg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/esp4.jpeg' data-holder-rendered='true'   style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'>Espaço de Festa ao Ar Livre</h1>
            
            <p class='lead'>Se localiza no jardim do restaurante, contaria também com pratos do nosso restaurante para lhe fornecer um melhor serviço. Possue uma decoração com muitas flores, mas também com mesas clássicas. Além disso, o local lhe fornece uma vista para a nossa fonte e lhe dá um local muito extensivo, onde caberia a maior quantidade de pessoas, e estaria protegido de chuvas ou outros imprevistos.</p>
             <a  href='visuE4.php'><button style='border-radius: 10px;background-color: #ececec;color: black;border: 1px solid black;'> Ver Dados</button> </a>
          
          </div>
        </div>
     
<br>
</div>
<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded float-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded float-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
            
</div>
</nav>

 




      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
  <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
  <script type='text/javascript' src='../bootstrap/js/pace.min.js'></script>
  
    </body>
  </html>";
;
  }else{
    echo"<!doctype html>
  <html lang='pt-br'> 
    <head>
      <!-- Required meta tags -->
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/bootstrap.css' />
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/espFesta.css' />
      <link rel='stylesheet' type='text/css' href='../bootstrap/css/style.css'/>
      <title> Reservar Espaço de Festa </title> <link  rel='shortcut icon'  href='../imagens/logo1.ico'  type='image/x-icon' >
    </head>
    <body style='background-color:#ececec;'>
      <div class='row topo'> 


        <div class='col-lg-12'> <img src='../imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


      </div>


  <nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
  <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
      <img src='../imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
    </button>

    <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
      <ul class='navbar-nav'>
        <li class='nav-item active'>
          <a class='nav-link px-md-4' href='../index.php' style='color: white;'>Home</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link text-white px-md-4' href='Cardapio.php'>Cardápio</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link text-white px-md-4' href='Atracoes.php'>Atrações </a>
        </li>
        <li class='nav-item dropdown'>
          <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            Reservar
          </a>
          <div class='dropdown-menu' aria-labelledby='dropdown08'>
            <a class='dropdown-item' href='Mesas.php'> Mesas </a>
            <a class='dropdown-item' href='EspFesta.php'> Espaço de Festa</a>
          </div>
        </li>

        <li class='nav-item dropdown'>
          <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            Entrar
          </a>
          <div class='dropdown-menu' aria-labelledby='dropdown08'>
            <a class='dropdown-item' href='Cadastro.php'> Cadastre - se </a>
            <a class='dropdown-item' href='Login.php'>  Login </a>
          </div>
        </li>
      </ul>
    </div>
  </nav>

  <div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
    <div class='carousel-inner'>
      <div class='carousel-item active'>
        <img src='../imagens/espFesta.png' class='d-block w-100' alt='...''>
      </div>

    </div>
  </div>

 <div class='container marketing'>
<h1 id='unc' class='display-4 text-center mt-3'>Usuário não Cadastrado! Cadastre-se no site, para ter acesso as reservas de nossos espaços.</h1>
 <br>
 <a href='Cadastro.php'><button class='btn btn-outline-danger btn-block mt-3 ml-3'> Cadastrar-se </button></a>
</div>
<br>
<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded float-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded float-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
            
</div>
</nav>

 




      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
  <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
  <script type='text/javascript' src='../bootstrap/js/pace.min.js'></script>
    </body>
  </html>";

  }
}
?>