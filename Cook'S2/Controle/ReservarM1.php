<?php
require_once("Conect.php");
require_once("../Modelo/ResevModelo.php");
    class ReservarM1{
  
        function inserir($mesa1){
            try{
                $conexao = new Conexao();
                $nuser = $mesa1->getNuser();
                $data = $mesa1->getData();
                $qtp = $mesa1->getQtp();
                $hora = $mesa1->getHora();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO mesa1(nomeU,dia,qtp,hora) VALUES(:n,:d,:qtp,:h);");
                $cmd->bindParam("n", $nuser);
                $cmd->bindParam("d",$data);
                $cmd->bindParam("qtp", $qtp);
                $cmd->bindParam("h", $hora);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
  }
    
}
?>





