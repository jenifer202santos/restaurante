<?php
require_once("Conect.php");
require_once("../Modelo/AdmModel.php");
class AdmControle{
            function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM adm;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "AdmModel");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function verificarLoginAdm($n,$s){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM adm WHERE user = :n AND senha = :s;");
                $cmd->bindParam("n", $n);
                $cmd->bindParam("s", $s);
                if($cmd->execute()){
                    if($cmd->rowCount() == 1){
                        return true;
                    }else{
                        return false;
                    }
                }
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function update($usuario ) {
       try {
                $conexao = new Conexao();
                $user = $usuario->getUser();
                $senha = $usuario->getSenha();
                $cmd= $conexao->getConexao()->prepare("UPDATE adm SET user= :n ,senha =:s WHERE id = 1;");
                $cmd->bindParam("n", $user);
                $cmd->bindParam("s", $senha);
          if($cmd->execute()){
             if($cmd->rowCount() > 0){
                return true;
             } else {
                return false;
             }
          } else {
             return false;
          }
       } catch ( PDOException $excecao ) {
          echo $excecao->getMessage();
       }
    }
    }
?> 