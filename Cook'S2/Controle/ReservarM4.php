<?php
require_once("Conect.php");
require_once("../Modelo/ResevModelo.php");
    class ReservarM4{
  
        function inserir($mesa4){
            try{
                $conexao = new Conexao();
                $nuser = $mesa4->getNuser();
                $data = $mesa4->getData();
                $qtp = $mesa4->getQtp();
                $hora = $mesa4->getHora();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO mesa4(nomeU,dia,quantP,hora) VALUES(:n,:d,:qtp,:h);");
                $cmd->bindParam("n", $nuser);
                $cmd->bindParam("d",$data);
                $cmd->bindParam("qtp", $qtp);
                $cmd->bindParam("h", $hora);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
  }
    
}
?>





