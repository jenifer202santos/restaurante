# Cook'S2

Nossa aplicação é sobre Restaurante.
O sistema tem como principal função a reserva de meses que permitirão uma vaga no restaurante.
É possível, também, atualizar as imagens de acordo com seus gostos, além disso, é possivel reservar sua mesa de forma rápida e efetiva.

# Funcionalidades

*  Sistema de Login e Cadastro
*  Reserva de mesas
*  Update de imagens
*  Update de textos
*  Tabela das Reservas
*  Nivel de acesso
*  Sessões


