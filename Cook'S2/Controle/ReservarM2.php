<?php
require_once("Conect.php");
require_once("../Modelo/ResevModelo.php");
    class ReservarM2{
  
        function inserir($mesa2){
            try{
                $conexao = new Conexao();
                $nuser = $mesa2->getNuser();
                $data = $mesa2->getData();
                $qtp = $mesa2->getQtp();
                $hora = $mesa2->getHora();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO mesa2(nomeU,dia,quantP,hora) VALUES(:n,:d,:qtp,:h);");
                $cmd->bindParam("n", $nuser);
                $cmd->bindParam("d",$data);
                $cmd->bindParam("qtp", $qtp);
                $cmd->bindParam("h", $hora);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
  }
    
}
?>





