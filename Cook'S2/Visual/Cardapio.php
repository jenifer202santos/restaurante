<?php
session_start();


if(isset($_SESSION['user'])){
 echo "<!doctype html>
<html lang='pt-br'> 
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <!-- Bootstrap CSS -->
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/cardapio.css' />
    <title> Cardápio </title> <link  rel='shortcut icon'  href='../imagens/logo1.ico'  type='image/x-icon' >
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/style.css'/>
  </head>
  <body style='background-color: #ececec;'>
  	<div class='row topo' style='background-color: white; whidth: 100%;'> 


  		<div class='col-lg-12' id='home'> <img src='../imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


  	</div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
    <img src='../imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
  </button>

  <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
    <ul class='navbar-nav'>
      <li class='nav-item active'>
        <a class='nav-link px-md-4' href='../index.php' style='color: white;'>Home</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='#'>Cardápio</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='Atracoes.php'>Atrações </a>
      </li>
      <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Reservar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='Mesas.php'> Mesas </a>
          <a class='dropdown-item' href='EspFesta.php'> Espaço de Festa</a>
        </div>
      </li>

      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='../Controle/sair.php'>Sair</a>
      </li>
    </ul>
  </div>
</nav>

<div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
  <div class='carousel-inner'>
    <div class='carousel-item active'>
      <img src='../imagens/prato.png' class='d-block w-100' alt='...'>
    </div>

  </div>
</div>

<div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Feijão Tropeiro</h1>
            <p class='lead'>O feijão-tropeiro é um prato típico da culinária Paulista, Mineira e Goiana. Criado por bandeirantes e tropeiros paulistas , composto de feijão misturado a fari nha de mandioca, torresmo, lingüiça, ovos, alho, cebola e temperos

É um prato tradicional também de outras regiões de influência bandeirante e colonização paulista (ou que já foram parte deste estado), como é o caso dos estados brasileiros de Goiás e Minas Gerais , aonde o prato foi introduzido pelos bandeirantes paulistas</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/prato.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>	

      <hr class='featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/moqueca.jpeg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Moqueca de Tilápia</h1>
            <p class='lead'>A origem da moqueca de peixe é um debate até os dias de hoje. Enquanto os baianos defendem que a receita original, composta por azeite de dendê e com leite de coco veio da Bahia, os capixabas possuem a sua própria história, ressaltando que a moqueca é um prato tradicional do Espírito Santo. 

Independente desse debate, é inegável que o sabor da moqueca de peixe é algo inigualável e que certamente é um prato único na culinária brasileira. Prato que, de certa forma, é um presente dos índios para a nossa população, pois vem desde essa época, onde o peixe era assado envolto em folhas, anteriormente conhecido como pokeka.</p>
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Baião de Dois com Carne Seca</h1>
            <p class='lead'>A história do Baião de Dois remonta aos tempos de grandes dificuldades do povo nordestino com as secas quando a comida era escassa e nada podia se estragar ou desperdiçar. Assim, o cearense uniu as sobras da cozinha, arroz e feijão com o pouco que tinha de carne seca e queijo de coalho e surgiu assim esse saboroso prato da cozinha nordestina. - Com o tempo as receitas foram sendo ampliadas e diversificadas, de modo que cada cearense tem a sua própria receita.A história do Baião de Dois remonta aos tempos de grandes dificuldades do povo nordestino com as secas quando a comida era escassa e nada podia se estragar ou desperdiçar. Assim, o cearense uniu as sobras da cozinha, arroz e feijão com o pouco que tinha de carne seca e queijo de coalho e surgiu assim esse saboroso prato da cozinha nordestina. - Com o tempo as receitas foram sendo ampliadas e diversificadas, de modo que cada cearense tem a sua própria receita.</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/baiao.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/farofa.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Farofa com Bacon e Calabresa</h1>
            <p class='lead'>Farofa (do quimbundo falofa) é a farinha de mandioca ou farinha de milho escaldadas ou torradas, geralmente passadas na gordura ou na manteiga, nas quais podem ser acrescentados inúmeros outros ingredientes, tais como: bacon torrado, linguiça frita, ovo, carne, tofu, ou outros alimentos de origem vegetal.É um prato bastante popular no Brasil, tendo sua origem registrada no período colonial. Serve de acompanhamento aos assados de carne, ave ou peixe. Por ter e ser um alimento de baixo custo e fácil de preparar, é muito comum entre os trabalhadores. Farofa pode ser encontrada em pacotes industrializados, mas também é muito preparada em casa, seguindo receitas familiares que variam com a região do país.<p>
          </div>
        </div>
    <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Feijoada</h1>
            <p class='lead'>Alguns historiadores dizem que a feijoada foi criada no Brasil no período da escravidão, quando os escravos recebiam as partes menos nobres do porco, como orelhas, rabos ou pés. Eles então juntavam esses os pedaços rejeitados e cozinhavam com feijão, adicionando água, sal e pimentas. Outros afirmam que, em Portugal, a feijoada tinha uma versão conhecida como cozido. Há relatos, no entanto, de que a mistura de feijão com carnes, legumes e verduras é conhecida desde os tempos do Império Romano. Enfim, o que sabemos mesmo é que é um prato incrivelmente maravilhoso!</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/feijoada.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
    <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/carnesol.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Carne do Sol</h1>
            <p class='lead'>Segundo vários estudiosos o processo de salgar e expor a carne ao sol é pré-histórico. É pouco provável que tenhamos herdado a técnica de produção de grupos indígenas, pois não é hábito dessas culturas conservar alimentos. Já os portugueses tinham tradição de conservar alimentos expondo-os ao sol (frutas) e salgando-os (peixes e bacalhau), provavelmente no litoral nordestino dos primeiros séculos da colonização (onde o sal e o sol eram abundantes) os pescadores passaram o processo dos peixes às carnes. <p>
          </div>
        </div>
    <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Acarajé </h1>
            <p class='lead'>A palavra acarajé tem origem africana e quer dizer 'comer bola de fogo' (akará, significa bola de fogo, e jé significa comer). O nome desse prato típico surgiu a partir de uma lenda de um casal Xangô (orixá dos raios, trovões, fogo e justiça) e sua esposa Iansã, uma deusa responsável pelos ventos e tempestades.A lenda diz que Iansã procurou Ifá, um oráculo africano, para buscar comida para o marido Xangô. O oráculo falou para a esposa que quando o marido comesse deveria contar para o povo. Ao partir, Iansã desconfiou e comeu antes de levar a ele, porém nada aconteceu. Quando chegou em casa, deu a comida a Xangô e disse as recomendações. Quando comeu foi contar ao povo e fogo saiu de sua boca. Iansã ficou nervosa e correu para ajudá-lo, mas de sua boca também saiu fogo e o povo começou a chamá-lo de grande rei de Oyó (do fogo).</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/acaraje.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
    <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/buchada.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Buchada de Carneiro</h1>
            <p class='lead'>Buchada de carneiro, é um prato típico da região Nordeste do Brasil. A iguaria tem origem no maranho, da culinária portuguesa tradicional. A buchada remete também ao prato crux mechi da gastronomia libanesa. <p>
          </div>
        </div>
<br>
</div>


 
<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded float-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded float-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
            <a href='#home'><p id='voltTop' class='lead text-white' style='margin-left: 40px;margin-top: 13px ;color: white'> Voltar ao Topo </p></a>
</div> 

</nav>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
<script type='text/javascript' src='../bootstrap/js/pace.min.js'></script>
  </body>
</html> ";

}else{
  if(isset($_SESSION['adm'])){
     echo "<!doctype html>
<html lang='pt-br'> 
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <!-- Bootstrap CSS -->
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/cardapio.css' />
    <title> Cardápio-Administrador </title> <link  rel='shortcut icon'  href='../imagens/logo1.ico'  type='image/x-icon' >
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/style.css'/>
  </head>
  <body style='background-color: #ececec;'>
    <div class='row topo' style='background-color: white; whidth: 100%;'> 


      <div class='col-lg-12' id='home'> <img src='../imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


    </div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
    <img src='../imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
  </button>

  <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
    <ul class='navbar-nav'>
      <li class='nav-item active'>
        <a class='nav-link px-md-4' href='../index.php' style='color: white;'>Home</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='#'>Cardápio</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='Atracoes.php'>Atrações </a>
      </li>
      <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Reservar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='Mesas.php'> Mesas </a>
          <a class='dropdown-item' href='EspFesta.php'> Espaço de Festa</a>
        </div>
      </li>

    <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='upAdm.php'>Modificar Adm</a>
      </li>
     <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='../Controle/sair.php'>Sair</a>
      </li>
    </ul>
  </div>
</nav>

<div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
  <div class='carousel-inner'>
    <div class='carousel-item active'>
      <img src='../imagens/prato.png' class='d-block w-100' alt='...'>
    </div>

  </div>
</div>

<div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Feijão Tropeiro</h1>
            <p class='lead'>O feijão-tropeiro é um prato típico da culinária Paulista, Mineira e Goiana. Criado por bandeirantes e tropeiros paulistas , composto de feijão misturado a fari nha de mandioca, torresmo, lingüiça, ovos, alho, cebola e temperos

É um prato tradicional também de outras regiões de influência bandeirante e colonização paulista (ou que já foram parte deste estado), como é o caso dos estados brasileiros de Goiás e Minas Gerais , aonde o prato foi introduzido pelos bandeirantes paulistas</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/prato.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>  

      <hr class='featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/moqueca.jpeg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Moqueca de Tilápia</h1>
            <p class='lead'>A origem da moqueca de peixe é um debate até os dias de hoje. Enquanto os baianos defendem que a receita original, composta por azeite de dendê e com leite de coco veio da Bahia, os capixabas possuem a sua própria história, ressaltando que a moqueca é um prato tradicional do Espírito Santo. 

Independente desse debate, é inegável que o sabor da moqueca de peixe é algo inigualável e que certamente é um prato único na culinária brasileira. Prato que, de certa forma, é um presente dos índios para a nossa população, pois vem desde essa época, onde o peixe era assado envolto em folhas, anteriormente conhecido como pokeka.</p>
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Baião de Dois com Carne Seca</h1>
            <p class='lead'>A história do Baião de Dois remonta aos tempos de grandes dificuldades do povo nordestino com as secas quando a comida era escassa e nada podia se estragar ou desperdiçar. Assim, o cearense uniu as sobras da cozinha, arroz e feijão com o pouco que tinha de carne seca e queijo de coalho e surgiu assim esse saboroso prato da cozinha nordestina. - Com o tempo as receitas foram sendo ampliadas e diversificadas, de modo que cada cearense tem a sua própria receita.A história do Baião de Dois remonta aos tempos de grandes dificuldades do povo nordestino com as secas quando a comida era escassa e nada podia se estragar ou desperdiçar. Assim, o cearense uniu as sobras da cozinha, arroz e feijão com o pouco que tinha de carne seca e queijo de coalho e surgiu assim esse saboroso prato da cozinha nordestina. - Com o tempo as receitas foram sendo ampliadas e diversificadas, de modo que cada cearense tem a sua própria receita.</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/baiao.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/farofa.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Farofa com Bacon e Calabresa</h1>
            <p class='lead'>Farofa (do quimbundo falofa) é a farinha de mandioca ou farinha de milho escaldadas ou torradas, geralmente passadas na gordura ou na manteiga, nas quais podem ser acrescentados inúmeros outros ingredientes, tais como: bacon torrado, linguiça frita, ovo, carne, tofu, ou outros alimentos de origem vegetal.É um prato bastante popular no Brasil, tendo sua origem registrada no período colonial. Serve de acompanhamento aos assados de carne, ave ou peixe. Por ter e ser um alimento de baixo custo e fácil de preparar, é muito comum entre os trabalhadores. Farofa pode ser encontrada em pacotes industrializados, mas também é muito preparada em casa, seguindo receitas familiares que variam com a região do país.<p>
          </div>
        </div>
    <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Feijoada</h1>
            <p class='lead'>Alguns historiadores dizem que a feijoada foi criada no Brasil no período da escravidão, quando os escravos recebiam as partes menos nobres do porco, como orelhas, rabos ou pés. Eles então juntavam esses os pedaços rejeitados e cozinhavam com feijão, adicionando água, sal e pimentas. Outros afirmam que, em Portugal, a feijoada tinha uma versão conhecida como cozido. Há relatos, no entanto, de que a mistura de feijão com carnes, legumes e verduras é conhecida desde os tempos do Império Romano. Enfim, o que sabemos mesmo é que é um prato incrivelmente maravilhoso!</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/feijoada.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
    <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/carnesol.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Carne do Sol</h1>
            <p class='lead'>Segundo vários estudiosos o processo de salgar e expor a carne ao sol é pré-histórico. É pouco provável que tenhamos herdado a técnica de produção de grupos indígenas, pois não é hábito dessas culturas conservar alimentos. Já os portugueses tinham tradição de conservar alimentos expondo-os ao sol (frutas) e salgando-os (peixes e bacalhau), provavelmente no litoral nordestino dos primeiros séculos da colonização (onde o sal e o sol eram abundantes) os pescadores passaram o processo dos peixes às carnes. <p>
          </div>
        </div>
    <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Acarajé </h1>
            <p class='lead'>A palavra acarajé tem origem africana e quer dizer 'comer bola de fogo' (akará, significa bola de fogo, e jé significa comer). O nome desse prato típico surgiu a partir de uma lenda de um casal Xangô (orixá dos raios, trovões, fogo e justiça) e sua esposa Iansã, uma deusa responsável pelos ventos e tempestades.A lenda diz que Iansã procurou Ifá, um oráculo africano, para buscar comida para o marido Xangô. O oráculo falou para a esposa que quando o marido comesse deveria contar para o povo. Ao partir, Iansã desconfiou e comeu antes de levar a ele, porém nada aconteceu. Quando chegou em casa, deu a comida a Xangô e disse as recomendações. Quando comeu foi contar ao povo e fogo saiu de sua boca. Iansã ficou nervosa e correu para ajudá-lo, mas de sua boca também saiu fogo e o povo começou a chamá-lo de grande rei de Oyó (do fogo).</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/acaraje.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
    <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/buchada.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Buchada de Carneiro</h1>
            <p class='lead'>Buchada de carneiro, é um prato típico da região Nordeste do Brasil. A iguaria tem origem no maranho, da culinária portuguesa tradicional. A buchada remete também ao prato crux mechi da gastronomia libanesa. <p>
          </div>
        </div>
<br>
</div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded float-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded float-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
             <a href='#home'><p id='voltTop' class='lead text-white' style='margin-left: 40px;margin-top: 13px ;color: white'> Voltar ao Topo </p></a>
            
</div>

 
</nav>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
<script type='text/javascript' src='../bootstrap/js/pace.min.js'></script>
  </body>
</html> ";


 

  }else{
 echo "<!doctype html>
<html lang='pt-br'> 
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <!-- Bootstrap CSS -->
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/cardapio.css' />
    <link rel='stylesheet' type='text/css' href='../bootstrap/css/style.css'/>
    <title> Cardápio </title> <link  rel='shortcut icon'  href='../imagens/logo1.ico'  type='image/x-icon' >
  </head>
  <body style='background-color: #ececec;'>
    <div class='row topo' style='background-color: white; whidth: 100%;'> 


      <div class='col-lg-12' id='home'> <img src='../imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


    </div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
    <img src='../imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
  </button>

  <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
    <ul class='navbar-nav'>
      <li class='nav-item active'>
        <a class='nav-link px-md-4' href='../index.php' style='color: white;'>Home</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='#'>Cardápio</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='Atracoes.php'>Atrações </a>
      </li>
      <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Reservar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='Mesas.php'> Mesas </a>
          <a class='dropdown-item' href='EspFesta.php'> Espaço de Festa</a>
        </div>
      </li>

   <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Entrar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='Cadastro.php'> Cadastre - se </a>
          <a class='dropdown-item' href='Login.php'>  Login </a>
        </div>
      </li>
    </ul>
  </div>
</nav>

<div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
  <div class='carousel-inner'>
    <div class='carousel-item active'>
      <img src='../imagens/prato.png' class='d-block w-100' alt='...'>
    </div>

  </div>
</div>

<div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Feijão Tropeiro</h1>
            <p class='lead'>O feijão-tropeiro é um prato típico da culinária Paulista, Mineira e Goiana. Criado por bandeirantes e tropeiros paulistas , composto de feijão misturado a fari nha de mandioca, torresmo, lingüiça, ovos, alho, cebola e temperos

É um prato tradicional também de outras regiões de influência bandeirante e colonização paulista (ou que já foram parte deste estado), como é o caso dos estados brasileiros de Goiás e Minas Gerais , aonde o prato foi introduzido pelos bandeirantes paulistas</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/prato.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>  

      <hr class='featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/moqueca.jpeg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Moqueca de Tilápia</h1>
            <p class='lead'>A origem da moqueca de peixe é um debate até os dias de hoje. Enquanto os baianos defendem que a receita original, composta por azeite de dendê e com leite de coco veio da Bahia, os capixabas possuem a sua própria história, ressaltando que a moqueca é um prato tradicional do Espírito Santo. 

Independente desse debate, é inegável que o sabor da moqueca de peixe é algo inigualável e que certamente é um prato único na culinária brasileira. Prato que, de certa forma, é um presente dos índios para a nossa população, pois vem desde essa época, onde o peixe era assado envolto em folhas, anteriormente conhecido como pokeka.</p>
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Baião de Dois com Carne Seca</h1>
            <p class='lead'>A história do Baião de Dois remonta aos tempos de grandes dificuldades do povo nordestino com as secas quando a comida era escassa e nada podia se estragar ou desperdiçar. Assim, o cearense uniu as sobras da cozinha, arroz e feijão com o pouco que tinha de carne seca e queijo de coalho e surgiu assim esse saboroso prato da cozinha nordestina. - Com o tempo as receitas foram sendo ampliadas e diversificadas, de modo que cada cearense tem a sua própria receita.A história do Baião de Dois remonta aos tempos de grandes dificuldades do povo nordestino com as secas quando a comida era escassa e nada podia se estragar ou desperdiçar. Assim, o cearense uniu as sobras da cozinha, arroz e feijão com o pouco que tinha de carne seca e queijo de coalho e surgiu assim esse saboroso prato da cozinha nordestina. - Com o tempo as receitas foram sendo ampliadas e diversificadas, de modo que cada cearense tem a sua própria receita.</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/baiao.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/farofa.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Farofa com Bacon e Calabresa</h1>
            <p class='lead'>Farofa (do quimbundo falofa) é a farinha de mandioca ou farinha de milho escaldadas ou torradas, geralmente passadas na gordura ou na manteiga, nas quais podem ser acrescentados inúmeros outros ingredientes, tais como: bacon torrado, linguiça frita, ovo, carne, tofu, ou outros alimentos de origem vegetal.É um prato bastante popular no Brasil, tendo sua origem registrada no período colonial. Serve de acompanhamento aos assados de carne, ave ou peixe. Por ter e ser um alimento de baixo custo e fácil de preparar, é muito comum entre os trabalhadores. Farofa pode ser encontrada em pacotes industrializados, mas também é muito preparada em casa, seguindo receitas familiares que variam com a região do país.<p>
          </div>
        </div>
    <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Feijoada</h1>
            <p class='lead'>Alguns historiadores dizem que a feijoada foi criada no Brasil no período da escravidão, quando os escravos recebiam as partes menos nobres do porco, como orelhas, rabos ou pés. Eles então juntavam esses os pedaços rejeitados e cozinhavam com feijão, adicionando água, sal e pimentas. Outros afirmam que, em Portugal, a feijoada tinha uma versão conhecida como cozido. Há relatos, no entanto, de que a mistura de feijão com carnes, legumes e verduras é conhecida desde os tempos do Império Romano. Enfim, o que sabemos mesmo é que é um prato incrivelmente maravilhoso!</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/feijoada.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
    <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/carnesol.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Carne do Sol</h1>
            <p class='lead'>Segundo vários estudiosos o processo de salgar e expor a carne ao sol é pré-histórico. É pouco provável que tenhamos herdado a técnica de produção de grupos indígenas, pois não é hábito dessas culturas conservar alimentos. Já os portugueses tinham tradição de conservar alimentos expondo-os ao sol (frutas) e salgando-os (peixes e bacalhau), provavelmente no litoral nordestino dos primeiros séculos da colonização (onde o sal e o sol eram abundantes) os pescadores passaram o processo dos peixes às carnes. <p>
          </div>
        </div>
    <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Acarajé </h1>
            <p class='lead'>A palavra acarajé tem origem africana e quer dizer 'comer bola de fogo' (akará, significa bola de fogo, e jé significa comer). O nome desse prato típico surgiu a partir de uma lenda de um casal Xangô (orixá dos raios, trovões, fogo e justiça) e sua esposa Iansã, uma deusa responsável pelos ventos e tempestades.A lenda diz que Iansã procurou Ifá, um oráculo africano, para buscar comida para o marido Xangô. O oráculo falou para a esposa que quando o marido comesse deveria contar para o povo. Ao partir, Iansã desconfiou e comeu antes de levar a ele, porém nada aconteceu. Quando chegou em casa, deu a comida a Xangô e disse as recomendações. Quando comeu foi contar ao povo e fogo saiu de sua boca. Iansã ficou nervosa e correu para ajudá-lo, mas de sua boca também saiu fogo e o povo começou a chamá-lo de grande rei de Oyó (do fogo).</p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/acaraje.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
        </div>
    <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto' data-src='holder.js/500x500/auto' alt='500x500' src='../imagens/buchada.jpg' data-holder-rendered='true' style='width: 450px; height: 450px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Buchada de Carneiro</h1>
            <p class='lead'>Buchada de carneiro, é um prato típico da região Nordeste do Brasil. A iguaria tem origem no maranho, da culinária portuguesa tradicional. A buchada remete também ao prato crux mechi da gastronomia libanesa. <p>
          </div>
        </div>
<br>
</div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded float-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='../imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded float-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='./imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
             <a href='#home'><p id='voltTop' class='lead text-white' style='margin-left: 40px;margin-top: 13px ;color: white'> Voltar ao Topo </p></a>
</div>

  
</nav>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
<script type='text/javascript' src='../bootstrap/js/pace.min.js'></script>
  </body>
</html> ";
  }
}
?>