<?php
    class ResevModelo{
        private $id;
        private $nuser;
        private $data;
        private $qtp;
        private $hora;

        public function getHora(){
            return $this->hora;
        }
        public function setHora($h){
            $this->hora = $h; 
        }

        public function getId(){
            return $this->id;
        }
        public function setId($i){
            $this->id = $i; 
        }

        public function getNuser(){
            return $this->nuser;
        }
        public function getData(){
            return $this->data;
        }
        public function getQtp(){
            return $this->qtp;
        }
        public function setQtp($qtp){
             $this->qtp = $qtp;;
        }

        public function setNuser($n){
            $this->nuser = $n;
        }
        public function setData($d){
            $this->data = $d;
        }
        
    }
?>  
