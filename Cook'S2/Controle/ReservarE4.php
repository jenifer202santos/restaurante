<?php
require_once("Conect.php");
require_once("../Modelo/EspaceModelo.php");
    class ReservarE4{
  
        function inserir4($e4){
            try{
                $conexao = new Conexao();
                $nuser = $e4->getNuser();
                $data = $e4->getData();
                $qtp = $e4->getQtp();
                $hora = $e4->getHora();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO esp4(nomeC,dia,duracao,quantP) VALUES(:n,:d,:h,:qtp);");
                $cmd->bindParam("n", $nuser);
                $cmd->bindParam("d",$data);
                $cmd->bindParam("qtp", $qtp);
                $cmd->bindParam("h", $hora);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
  }
    
}
?>





