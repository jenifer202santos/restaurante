<?php
require_once("Conect.php");
require_once("../Modelo/ResevModelo.php");
    class ReservarM3{
  
        function inserir($mesa3){
            try{
                $conexao = new Conexao();
                $nuser = $mesa3->getNuser();
                $data = $mesa3->getData();
                $qtp = $mesa3->getQtp();
                $hora = $mesa3->getHora();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO mesa3(nomeU,dia,quantP,hora) VALUES(:n,:d,:qtp,:h);");
                $cmd->bindParam("n", $nuser);
                $cmd->bindParam("d",$data);
                $cmd->bindParam("qtp", $qtp);
                $cmd->bindParam("h", $hora);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
  }
    
}
?>





