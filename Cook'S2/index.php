<?php

session_start();


if(isset($_SESSION['user'])){
echo"<!doctype html>
<html lang='pt-br'> 
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <!-- Bootstrap CSS -->
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/index.css' />
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/style.css'/>
    <title> Home</title> <link  rel='shortcut icon'  href='./imagens/logo1.ico'  type='image/x-icon' >
  </head>
  <body style='background-color:#ececec;'>
  	<div class='row topo'> 


  		<div class='col-lg-12' id='home'> <img src='./imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


  	</div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
    <img src='./imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
  </button>

  <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
    <ul class='navbar-nav'>
      <li class='nav-item active'>
        <a class='nav-link px-md-4' href='#' style='color: white;'>Home</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Visual/Cardapio.php'>Cardápio</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Visual/Atracoes.php'>Atrações </a>
      </li>
      <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Reservar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='./Visual/Mesas.php'> Mesas </a>
          <a class='dropdown-item' href='./Visual/EspFesta.php'> Espaço de Festa</a>
        </div>
      </li>
       <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Controle/sair.php'>Sair</a>
      </li>
    </ul>
  </div>
</nav>

<div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
  <ol class='carousel-indicators'>
    <li data-target='#carouselExampleIndicators' data-slide-to='0' class='active'></li>
    <li data-target='#carouselExampleIndicators' data-slide-to='1'></li>
    <li data-target='#carouselExampleIndicators' data-slide-to='2'></li>
  </ol>
  <div class='carousel-inner'>
    <div class='carousel-item active'>
      <img src='./imagens/banner1.png' class='d-block w-100' alt='...'>
    </div>
    <div class='carousel-item'>
      <img src='./imagens/banner2.png' class='d-block w-100' alt='...'>
    </div>
    <div class='carousel-item'>
      <img src='./imagens/banner3.png' class='d-block w-100' alt='...''>
    </div>
  </div>
  <a class='carousel-control-prev' href='#carouselExampleIndicators' role='button' data-slide='prev'>
    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
    <span class='sr-only'>Previous</span>
  </a>
  <a class='carousel-control-next' href='#carouselExampleIndicators' role='button' data-slide='next'>
    <span class='carousel-control-next-icon' aria-hidden='true'></span>
    <span class='sr-only'>Next</span>
  </a>
</div>

<div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Cardápio</h1>
            <p class='lead'> Onde se localiza a variabilidade da culinária brasileira, aproveite para ficar com água na boca com nossos pratos e conheça a origem de cada prato, pois temos cultura.</p>
            <p class='lead'></p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle rounded-circlerounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/menu.jpg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
        </div>  

      <hr class=1featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/at1.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Atrações</h1>
            <p class='lead'> Mostra as atrações disponíveis e principais da semana, a qual pode ser atualizadas pelo dono do restaurante com o objetivo de trazer mais diversidade.</p>
             <p class='lead'></p>
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
            <h1 class='featurette-heading'> Reservas de Mesas</h1>
            <p class='lead'> Você pode reservar antercipadamente nossas mesas, cada uma com sua personalidade própria.</p>
             <p class='lead'></p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/mes5.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
             <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/esp6.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
            <h1 class='featurette-heading'> Espaço de Festas</h1>
            <p class='lead'> Você pode reservar antercipadamente os melhores espaços de festa da região, com preços assessíveis e espaço amplo para qualquer tipo de tema.<p>
            <p class='lead'></p>
          </div>
        </div>
<br>
</div>

<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded rounded-circlefloat-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='./imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded rounded-circlefloat-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='./imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
             <a href='#home'><p id='voltTop' class='lead text-white' style='margin-left: 40px;margin-top: 13px ;color: white'> Voltar ao Topo </p></a>
            
</div>

</nav>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
<script type='text/javascript' src='./bootstrap/js/pace.min.js'></script>
  </body>
</html>";
}else{
  if(isset($_SESSION['adm'])){
    echo"<!doctype html>
<html lang='pt-br'> 
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <!-- Bootstrap CSS -->
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/index.css' />
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/style.css'/>
    <title> Home - Adminstrador</title> <link  rel='shortcut icon'  href='./imagens/logo1.ico'  type='image/x-icon' >
  </head>
  <body style='background-color:#ececec;'>
    <div class='row topo'> 


      <div class='col-lg-12' id='home'> <img src='./imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


    </div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
    <img src='./imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
  </button>

  <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
    <ul class='navbar-nav'>
      <li class='nav-item active'>
        <a class='nav-link px-md-4' href='#' style='color: white;'>Home</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Visual/Cardapio.php'>Cardápio</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Visual/Atracoes.php'>Atrações </a>
      </li>
      <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Reservar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='./Visual/Mesas.php'> Mesas </a>
          <a class='dropdown-item' href='./Visual/EspFesta.php'> Espaço de Festa</a>
        </div>
      </li>
       <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Visual/upAdm.php'>Modificar Adm</a>
      </li>
     <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='../Controle/sair.php'>Sair</a>
      </li>
    </ul>
  </div>
</nav>

<div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
  <ol class='carousel-indicators'>
    <li data-target='#carouselExampleIndicators' data-slide-to='0' class='active'></li>
    <li data-target='#carouselExampleIndicators' data-slide-to='1'></li>
    <li data-target='#carouselExampleIndicators' data-slide-to='2'></li>
  </ol>
  <div class='carousel-inner'>
    <div class='carousel-item active'>
      <img src='./imagens/banner1.png' class='d-block w-100' alt='...'>
    </div>
    <div class='carousel-item'>
      <img src='./imagens/banner2.png' class='d-block w-100' alt='...'>
    </div>
    <div class='carousel-item'>
      <img src='./imagens/banner3.png' class='d-block w-100' alt='...''>
    </div>
  </div>
  <a class='carousel-control-prev' href='#carouselExampleIndicators' role='button' data-slide='prev'>
    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
    <span class='sr-only'>Previous</span>
  </a>
  <a class='carousel-control-next' href='#carouselExampleIndicators' role='button' data-slide='next'>
    <span class='carousel-control-next-icon' aria-hidden='true'></span>
    <span class='sr-only'>Next</span>
  </a>
</div>

<div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
             <h1 class='featurette-heading'> Cardápio</h1>
            <p class='lead'> Onde se localiza a variabilidade da culinária brasileira, aproveite para ficar com água na boca com nossos pratos e conheça a origem de cada prato, pois temos cultura.</p>
            <p class='lead'></p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/menu.jpg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
        </div>  

      <hr class=1featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/at1.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
             <h1 class='featurette-heading'> Atrações</h1>
            <p class='lead'> Mostra as atrações disponíveis e principais da semana, a qual pode ser atualizadas pelo dono do restaurante com o objetivo de trazer mais diversidade.</p>
             <p class='lead'></p>
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
             <h1 class='featurette-heading'> Reservas de Mesas</h1>
            <p class='lead'> Você pode reservar antercipadamente nossas mesas, cada uma com sua personalidade própria.</p>
             <p class='lead'></p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/mes5.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
             <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/esp6.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
           <h1 class='featurette-heading'> Espaço de Festas</h1>
            <p class='lead'> Você pode reservar antercipadamente os melhores espaços de festa da região, com preços assessíveis e espaço amplo para qualquer tipo de tema.<p>
            <p class='lead'></p>
          </div>
        </div>
<br>
</div>

<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded rounded-circlefloat-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='./imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded rounded-circlefloat-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='./imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
             <a href='#home'><p id='voltTop' class='lead text-white' style='margin-left: 40px;margin-top: 13px ;color: white'> Voltar ao Topo </p></a>
            
</div>
</nav>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
<script type='text/javascript' src='./bootstrap/js/pace.min.js'></script>
  </body>
</html>";
    }else{
      echo"<!doctype html>
<html lang='pt-br'> 
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <!-- Bootstrap CSS -->
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/index.css' />
    <link rel='stylesheet' type='text/css' href='./bootstrap/css/style.css'/>
    <title> Home</title> <link  rel='shortcut icon'  href='./imagens/logo1.ico'  type='image/x-icon' >
  </head>
  <body style='background-color:#ececec;'>
    <div class='row topo'> 


      <div class='col-lg-12' id='home'> <img src='./imagens/logo2.png' class='rounded mx-auto d-block' alt='Logo'/></div>


    </div>


<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>
<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExample08' aria-controls='navbarsExample08' aria-expanded='false' aria-label='Alternar de navegação'>
    <img src='./imagens/icon.png' /><label style='color: white;'>Menu</label><span class='navbar-toggler-icon'></span>
  </button>

  <div class='collapse navbar-collapse justify-content-md-center' id='navbarsExample08'>
    <ul class='navbar-nav'>
      <li class='nav-item active'>
        <a class='nav-link px-md-4' href='#' style='color: white;'>Home</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Visual/Cardapio.php'>Cardápio</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link text-white px-md-4' href='./Visual/Atracoes.php'>Atrações </a>
      </li>
      <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Reservar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='./Visual/Mesas.php'> Mesas </a>
          <a class='dropdown-item' href='./Visual/EspFesta.php'> Espaço de Festa</a>
        </div>
      </li>
      <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle text-white px-md-4' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Entrar
        </a>
        <div class='dropdown-menu' aria-labelledby='dropdown08'>
          <a class='dropdown-item' href='./Visual/Cadastro.php'> Cadastre - se </a>
          <a class='dropdown-item' href='./Visual/Login.php'>  Login </a>
        </div>
      </li>
    </ul>
  </div>
</nav>

<div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel' interval='5000'>
  <ol class='carousel-indicators'>
    <li data-target='#carouselExampleIndicators' data-slide-to='0' class='active'></li>
    <li data-target='#carouselExampleIndicators' data-slide-to='1'></li>
    <li data-target='#carouselExampleIndicators' data-slide-to='2'></li>
  </ol>
  <div class='carousel-inner'>
    <div class='carousel-item active'>
      <img src='./imagens/banner1.png' class='d-block w-100' alt='...'>
    </div>
    <div class='carousel-item'>
      <img src='./imagens/banner2.png' class='d-block w-100' alt='...'>
    </div>
    <div class='carousel-item'>
      <img src='./imagens/banner3.png' class='d-block w-100' alt='...''>
    </div>
  </div>
  <a class='carousel-control-prev' href='#carouselExampleIndicators' role='button' data-slide='prev'>
    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
    <span class='sr-only'>Previous</span>
  </a>
  <a class='carousel-control-next' href='#carouselExampleIndicators' role='button' data-slide='next'>
    <span class='carousel-control-next-icon' aria-hidden='true'></span>
    <span class='sr-only'>Next</span>
  </a>
</div>

<div class='container marketing'>
  <br>
  <div class='row featurette'>
          <div class='col-md-6'>
             <h1 class='featurette-heading'> Cardápio</h1>
            <p class='lead'> Onde se localiza a variabilidade da culinária brasileira, aproveite para ficar com água na boca com nossos pratos e conheça a origem de cada prato, pois temos cultura.</p>
            <p class='lead'></p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/menu.jpg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
        </div>  

      <hr class=1featurette-divider'>
  <div class='row featurette'>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle ' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/at1.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
             <h1 class='featurette-heading'> Atrações</h1>
            <p class='lead'> Mostra as atrações disponíveis e principais da semana, a qual pode ser atualizadas pelo dono do restaurante com o objetivo de trazer mais diversidade.</p>
             <p class='lead'></p>
          </div>
        </div>  
      <hr class='featurette-divider'>
      <div class='row featurette'>
          <div class='col-md-6'>
             <h1 class='featurette-heading'> Reservas de Mesas</h1>
            <p class='lead'> Você pode reservar antercipadamente nossas mesas, cada uma com sua personalidade própria.</p>
             <p class='lead'></p>
          </div>
          <div class='col-md-6'>
            <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/mes5.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
        </div>
      <hr class='featurette-divider'>
          <div class='row featurette'>
          <div class='col-md-6'>
             <img class='featurette-image img-fluid mx-auto rounded rounded-circle' data-src='holder.js/500x500/auto' alt='500x500' src='./imagens/esp6.jpeg' data-holder-rendered='true' style='width: 350px; height: 350px;'>
          </div>
      <div class='col-md-6'>
           <h1 class='featurette-heading'> Espaço de Festas</h1>
            <p class='lead'> Você pode reservar antercipadamente os melhores espaços de festa da região, com preços assessíveis e espaço amplo para qualquer tipo de tema.<p>
            <p class='lead'></p>
          </div>
        </div>
<br>
</div>

<nav class='navbar navbar-expand-lg' style='background-color: #280000;'>

 <p style='color: white'> Contatos :  </p> 
 <div class='row'>
  
            <a href='https://www.instagram.com/cook_s2/' style='margin-left: 40px;margin-top: 13px; color: white' > <img class='rounded rounded-circlefloat-left data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='./imagens/insta.png' data-holder-rendered='true'> @cook_s2  </a>

            <a href='https://www.google.com/intl/pt_pt/gmail/about/' style='margin-left: 40px;margin-top: 13px ;color: white' ><img class='rounded rounded-circlefloat-left' data-src='holder.js/400x350/auto' alt='400x350'   style='width: 22px; height: 22px;' src='./imagens/gmail.png' data-holder-rendered='true'>cooks2dev@gmail.com </a>
             <a href='#home'><p id='voltTop' class='lead text-white' style='margin-left: 40px;margin-top: 13px ;color: white'> Voltar ao Topo </p></a>
            
</div>
</nav>

  <script type='text/javascript' src='./bootstrap/js/pace.min.js'></script>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
  </body>
</html>";

    }
  }
?>